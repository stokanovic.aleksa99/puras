# Feature: LCD Digit

## Scenario_1:

**Given**: There is a function print_LCD_digit(number)<br>
**When**: that funcion has an argument 0 <br>
**Then**: function should return " _ \n| |\n|_|\n" <br>

## Scenario_2:

**Given**: There is a function print_LCD_digit(number)<br>
**When**: that funcion has an argument 1 <br>
**Then**: function should return "   \n  |\n  |\n" <br>

## Scenario_3:

**Given**: There is a function print_LCD_digit(number)<br>
**When**: that funcion has an argument 2 <br>
**Then**: function should return " _ \n _|\n|_ \n" <br>

## Scenario_4:

**Given**: There is a function print_LCD_digit(number)<br>
**When**: that funcion has an argument 3 <br>
**Then**: function should return " _ \n _|\n_|\n" <br>

## Scenario_5:

**Given**: There is a function print_LCD_digit(number)<br>
**When**: that funcion has an argument 4 <br>
**Then**: function should return "   \n|_|\n|  |\n" <br>

## Scenario_6:

**Given**: There is a function print_LCD_digit(number)<br>
**When**: that funcion has an argument 5 <br>
**Then**: function should return " _ \n|_\n _|\n" <br>

## Scenario_7:

**Given**: There is a function print_LCD_digit(number)<br>
**When**: that funcion has an argument 6 <br>
**Then**: function should return " _ \n|_ \n|_|\n" <br>

## Scenario_8:

**Given**: There is a function print_LCD_digit(number)<br>
**When**: that funcion has an argument 7 <br>
**Then**: function should return " _ \n  |\n  |\n" <br>

## Scenario_9:

**Given**: There is a function print_LCD_digit(number)<br>
**When**: that funcion has an argument 8 <br>
**Then**: function should return " _ \n|_|\n|_|\n" <br>

## Scenario_10:

**Given**: There is a function print_LCD_digit(number)<br>
**When**: that funcion has an argument 9 <br>
**Then**: function should return " _ \n|_|\n  |\n" <br>

## Scenario_11:

**Given**: There is a function print_LCD_digit(number)<br>
**When**: that funcion has an argument 12 <br>
**Then**: function should return "    _ \n  | _|\n  ||_ \n" <br>

## Scenario_12:

**Given**: There is a function print_LCD_digit(number)<br>
**When**: that funcion has an argument 34567 <br>
**Then**: function should return " _     _  _  _ \n _||_||_ |_   |\n _|  | _||_|  |\n" <br>

## Scenario_13:

**Given**: There is a function print_LCD_digit(number)<br>
**When**: that funcion has negative number as an argument<br>
**Then**: function should return "ERROR: Negative number is not supported." <br>
