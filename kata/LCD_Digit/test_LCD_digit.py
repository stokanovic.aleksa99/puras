"""Test modul for LCD Digit"""

import unittest

from LCD_Digit import LCD_Digit

class TestLCDDigits(unittest.TestCase):

    def test_should_print_zero_digit_LCD(self):
        #Testing Scenario 1
        self.assertEqual(" _ \n| |\n|_|\n", LCD_Digit().print_LCD_digit(0))

    def test_should_print_one_digit_LCD(self):
        #Testing Scenario 2
        self.assertEqual("   \n  |\n  |\n", LCD_Digit().print_LCD_digit(1))
    
    def test_should_print_two_digit_LCD(self):
        #Testing Scenario 3
        self.assertEqual(" _ \n _|\n|_ \n", LCD_Digit().print_LCD_digit(2))
    
    def test_should_print_three_digit_LCD(self):
        #Testing Scenario 4
        self.assertEqual(" _ \n _|\n _|\n", LCD_Digit().print_LCD_digit(3))

    def test_should_print_four_digit_LCD(self):
        #Testing Scenario 5
        self.assertEqual("   \n|_|\n  |\n", LCD_Digit().print_LCD_digit(4))

    def test_should_print_five_digit_LCD(self):
        #Testing Scenario 6
        self.assertEqual(" _ \n|_ \n _|\n", LCD_Digit().print_LCD_digit(5))

    def test_should_print_six_digit_LCD(self):
        #Testing Scenario 7
        self.assertEqual(" _ \n|_ \n|_|\n", LCD_Digit().print_LCD_digit(6))

    def test_should_print_seven_digit_LCD(self):
        #Testing Scenario 8
        self.assertEqual(" _ \n  |\n  |\n", LCD_Digit().print_LCD_digit(7))

    def test_should_print_eight_digit_LCD(self):
        #Testing Scenario 9
        self.assertEqual(" _ \n|_|\n|_|\n", LCD_Digit().print_LCD_digit(8))

    def test_should_print_nine_digit_LCD(self):
        #Testing Scenario 10
        self.assertEqual(" _ \n|_|\n  |\n", LCD_Digit().print_LCD_digit(9))

    def test_should_print_two_digit_number_LCD(self):
        #Testing Scenario 11
        self.assertEqual("    _ \n  | _|\n  ||_ \n", LCD_Digit().print_LCD_digit(12))

    def test_should_print_five_digit_number_LCD(self):
        #Testing Scenario 12
        self.assertEqual(" _     _  _  _ \n _||_||_ |_   |\n _|  | _||_|  |\n", LCD_Digit().print_LCD_digit(34567))

    def test_should_reject_negative_number(self):
        #Testing Scenario 13
        self.assertEqual("ERROR: Negative number is not supported.", LCD_Digit().print_LCD_digit(-1))

unittest.main()
