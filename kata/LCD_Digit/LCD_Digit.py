""""LCD Digit Modul"""

class LCD_Digit:

    #lines of Digit
    first_line = ""
    second_line = ""
    third_line = ""
    complete_digit = ""

    #LCD_zero
    zero_first_line = " _ "
    zero_second_line = "| |"
    zero_third_line = "|_|"

    #LCD_one
    one_first_line = "   "
    one_second_line = "  |"
    one_third_line = "  |"

    #LCD_two
    two_first_line = " _ "
    two_second_line = " _|"
    two_third_line = "|_ "

    #LCD_three
    three_first_line = " _ "
    three_second_line = " _|"
    three_third_line = " _|"

    #LCD_four
    four_first_line = "   "
    four_second_line = "|_|"
    four_third_line = "  |"

    #LCD_five
    five_first_line = " _ "
    five_second_line = "|_ "
    five_third_line = " _|"

    #LCD_six
    six_first_line = " _ "
    six_second_line = "|_ "
    six_third_line = "|_|"

    #LCD_seven
    seven_first_line = " _ "
    seven_second_line = "  |"
    seven_third_line = "  |"

    #LCD_eight
    eight_first_line = " _ "
    eight_second_line = "|_|"
    eight_third_line = "|_|"

    #LCD_nine 
    nine_first_line = " _ "
    nine_second_line = "|_|"
    nine_third_line = "  |"

    def print_LCD_digit(self, number):

        if number < 0:
            return "ERROR: Negative number is not supported."

        strNum = str(number)

        for i in range((len(str(number)))):
            if strNum[i] == "0":
                self.first_line += self.zero_first_line
                self.second_line += self.zero_second_line
                self.third_line += self.zero_third_line
            if strNum[i] == "1":
                self.first_line += self.one_first_line
                self.second_line += self.one_second_line
                self.third_line += self.one_third_line
            if strNum[i] == "2":
                self.first_line += self.two_first_line
                self.second_line += self.two_second_line
                self.third_line += self.two_third_line
            if strNum[i] == "3":
                self.first_line += self.three_first_line
                self.second_line += self.three_second_line
                self.third_line += self.three_third_line
            if strNum[i] == "4":
                self.first_line += self.four_first_line
                self.second_line += self.four_second_line
                self.third_line += self.four_third_line
            if strNum[i] == "5":
                self.first_line += self.five_first_line
                self.second_line += self.five_second_line
                self.third_line += self.five_third_line
            if strNum[i] == "6":
                self.first_line += self.six_first_line
                self.second_line += self.six_second_line
                self.third_line += self.six_third_line
            if strNum[i] == "7":
                self.first_line += self.seven_first_line
                self.second_line += self.seven_second_line
                self.third_line += self.seven_third_line
            if strNum[i] == "8":
                self.first_line += self.eight_first_line
                self.second_line += self.eight_second_line
                self.third_line += self.eight_third_line
            if strNum[i] == "9":
                self.first_line += self.nine_first_line
                self.second_line += self.nine_second_line
                self.third_line += self.nine_third_line
        
        #add end of lines after foor loop
        self.first_line = self.first_line + "\n"
        self.second_line = self.second_line + "\n"
        self.third_line = self.third_line + "\n"

        self.complete_digit = self.first_line + self.second_line + self.third_line
        return self.complete_digit 
